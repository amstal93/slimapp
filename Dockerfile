FROM alpine:latest

RUN apk --no-cache upgrade

ADD build/slimapp /slimapp

CMD ["/slimapp"]
