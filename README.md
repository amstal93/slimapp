# Slimapp

SlimApp from https://gitlab.com/bella.network/slimapp provides a simple HTTP webserver based on Go which prints all requested headers and data back to the user. In addition, every request will be logged to stdout. Program listens on port 8080.

Useful as example placeholder image which should be replaced with something else. Image will be refreshed at least monthly with the newest Alpine with installed updates and Go version.
